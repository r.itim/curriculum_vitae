jQuery(function(){

    jQuery(".navbar a, footer a").on("click", function(event){
    
        event.preventDefault();
        var hash = this.hash;
        
        jQuery('body,html').animate({scrollTop: jQuery(hash).offset().top} , 900 , function(){window.location.hash = hash;})
        
    });

})